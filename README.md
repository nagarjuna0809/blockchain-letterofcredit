# letterofcredit

Letter Of Credit Workflow with blockchain

## Manage Application
#### Start the existing Business Network
````
./start.sh
````
#### Stop
````
./stop.sh
````
#### Upgrade version of Business Network deploy
````
./upgrade.sh
````


#### Bank Creation Paylod
````
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ \ 
   "$class": "com.cts.planb.blockchain.lc.Bank", \ 
   "name": "ACBank", \ 
   "partyId": "acb" \ 
 }' 'http://35.224.53.118:3000/api/com.cts.planb.blockchain.lc.Bank'
````

#### Customer Creation Payload
````
curl -X PUT --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ \ 
   "$class": "com.cts.planb.blockchain.lc.Customer", \ 
   "firstname": "Apple", \ 
   "lastName": "Supplier", \ 
   "type": "SUPPLIER", \ 
   "bank": "resource:com.cts.planb.blockchain.lc.Bank#bca" \ 
 }' 'http://35.224.53.118:3000/api/com.cts.planb.blockchain.lc.Customer/apple'
````
#### Purchase Order Payload
````
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ \ 
   "$class": "com.cts.planb.blockchain.lc.PurchaseOrder", \ 
   "poId": "1", \ 
   "productDetails": "asdasdd" \ 
 }' 'http://35.224.53.118:3000/api/com.cts.planb.blockchain.lc.PurchaseOrder'
````
#### LC Creation or LC Request To Bank
````
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ \ 
   "$class": "com.cts.planb.blockchain.lc.RequestToBank", \ 
 "letterId": "computer_po_lc_1", \ 
   "retailer": "resource:com.cts.planb.blockchain.lc.Customer#walmart", \ 
   "supplier": "resource:com.cts.planb.blockchain.lc.Customer#apple", \ 
   "po": "resource:com.cts.planb.blockchain.lc.PurchaseOrder#1" \ 
  \ 
 }' 'http://35.224.53.118:3000/api/com.cts.planb.blockchain.lc.RequestToBank'
````
