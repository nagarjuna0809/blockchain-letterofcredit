/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * PlanB - Letter of Credit Business Model
 *
 * @authors Sandeep Kumar Boda, Nagarjuna Bandamedi, JEGANATHAN Subramanian
 */

namespace com.cts.planb.blockchain.lc

enum LetterStatus {
    o AWAITING_LC_CREATION
    o CANCEL
    o LC_REQ_INIT
    o LC_AMEND
    o SHIPMENT_INIT
    o SHIPPED
    o PAYMENT_ADVICE
    o PAYMENT_REQ
    o FUNDS_APPROVED
}

enum CustomerType {
    o SUPPLIER
    o RETAILER
}

asset PurchaseOrder identified by poId {
    o String poId
    o String productDetails
}

asset LetterOfCredit identified by letterId {
    o String letterId
    --> Customer retailer
    --> Customer supplier
    --> Bank issuingBank
    --> Bank advisingBank
    --> Party approver optional
    o LetterStatus status
    --> PurchaseOrder po
    o String latestComment optional
    o String instructions optional
    o Attachment[] attachments optional
}

concept Attachment {
    o String name
    o String data
    o String who
    o DateTime when optional
    o String status optional
    o String type
}

abstract participant Party identified by partyId {
    o String partyId
}

participant Bank extends Party {
    o String name
}

participant Customer extends Party {
    o String firstname
    o String lastName
    o CustomerType type
    --> Bank bank
}

transaction RequestToBank {
    o String letterId
    --> Customer retailer
    --> Customer supplier
    --> PurchaseOrder po
    o Attachment attachment
}

event RequestToBankEvent {
    --> LetterOfCredit lc
    --> Customer approvingParty
}

transaction LCCancel {
    --> LetterOfCredit lc
    --> Customer approvingParty
    o String cancelReason
}

event LCCancelEvent {
    --> LetterOfCredit lc
    --> Customer cancelParty
    o String cancelReason
}

transaction LCAccepted {
    --> LetterOfCredit lc
    --> Bank approvingParty
    o String instructions
    o Attachment attachment optional
}

event LCAcceptedEvent {
    --> LetterOfCredit lc
    --> Bank approvingParty
}

event LCInstructionsToAdvisingBankEvent {
    --> LetterOfCredit lc
    --> Bank approvingParty
}

transaction LCPassInstructionsToBeneficiary {
    --> LetterOfCredit lc
    --> Bank approvingParty
    o Attachment attachment optional
}

transaction AmendLC {
    --> LetterOfCredit lc
    --> Bank approvingParty
    o String amendReason
}

event AmendLCEvent {
    --> LetterOfCredit lc
    --> Bank amendReqParty
    o String amendReason
}

event LCPassInstructionsToBeneficiaryEvent {
    --> LetterOfCredit lc
    --> Bank approvingParty
}

transaction SubmitAttachmentsForReview {
    --> LetterOfCredit lc
    --> Customer approvingParty
    o Attachment[] attachments
}

event SubmitAttachmentsForReviewEvent {
    --> LetterOfCredit lc
    --> Customer approvingParty
}

transaction DebitAdviceToIssuingBank {
    --> LetterOfCredit lc
    --> Bank approvingParty
}

event DebitAdviceToIssuingBankEvent {
    --> LetterOfCredit lc
    --> Bank approvingParty
}

event PaymentIntoSupplierAccEvent {
    --> Customer supplier
    --> Bank approvingParty
}

transaction SendDebitAdviceToBuyer {
    --> LetterOfCredit lc
    --> Bank approvingParty
}

event SendDebitAdviceToBuyerEvent {
    --> LetterOfCredit lc
    --> Bank approvingParty
}

transaction ReviewAndApprovePayment {
    --> LetterOfCredit lc
    --> Customer approvingParty
}

event ReviewAndApprovePaymentEvent {
    --> LetterOfCredit lc
    --> Customer approvingParty
}

