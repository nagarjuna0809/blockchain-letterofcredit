#!/bin/sh
APP_VERSION=$1
echo $APP_VERSION
awk '{gsub(/\"version\": \".*?\"/,"\"version\": \"'${APP_VERSION}'\"")}1' package.json > package.json_tmp &&  mv package.json_tmp package.json

# npm version $APP_VERSION -m 'v$APP_VERSION' --allow-same-version
composer archive create -t dir --archiveFile letterofcredit@$APP_VERSION.bna -n .
# composer network install -a letterofcredit@$APP_VERSION.bna -c PeeraAdmin@hlfv1
# composer network install -a letterofcredit@$APP_VERSION.bna -c letterofcredit@$APP_VERSION
composer network install -a letterofcredit@$APP_VERSION.bna --card PeerAdmin@hlfv1
composer network upgrade -c PeerAdmin@hlfv1 -n letterofcredit -V $APP_VERSION
#composer-rest-server -c admin@letterofcredit
./stop.sh
./start.sh
