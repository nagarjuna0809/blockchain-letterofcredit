/**
* Retailer reviews debit advise request from Issuing bank and approves.
* 
* @param {com.cts.planb.blockchain.lc.ReviewAndApprovePayment} reviewAndApprovePayment
* @transaction
*/
async function reviewAndApprovePayment(txModel) {
    if(txModel.lc.status === 'PAYMENT_REQ') {
    	const factory = getFactory();
    	const namespace = 'com.cts.planb.blockchain.lc';
        if(txModel.approvingParty.getType() === 'Customer' && txModel.lc.approver.getType() === 'Customer'
        	&& txModel.lc.approver.getIdentifier() === txModel.approvingParty.getIdentifier()) {
            txModel.lc.status = 'FUNDS_APPROVED';
            txModel.lc.approver = null;
            const assetRegistry = await getAssetRegistry(txModel.lc.getFullyQualifiedType());
            await assetRegistry.update(txModel.lc);
        
            const event = factory.newEvent(namespace, 'ReviewAndApprovePaymentEvent');
            event.lc = factory.newRelationship(namespace, 'LetterOfCredit', txModel.lc.getIdentifier());
            event.approvingParty = factory.newRelationship(namespace, 'Customer', txModel.approvingParty.getIdentifier());
            emit(event);
        } else {
        	throw new Error('Access Denied, Only Retailer can review debit advise request from Issuing bank and can approve.');
        }
    } else {
    	throw new Error('LC Illegal State');
    }
}
