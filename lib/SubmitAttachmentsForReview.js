/**
* Supplier submit attachments to LC and sends it to advising bank for review.
* 
* @param {com.cts.planb.blockchain.lc.SubmitAttachmentsForReview} submitAttachmentsForReview
* @transaction
*/
async function submitAttachmentsForReview(txModel) {
    if(txModel.lc.status === 'SHIPMENT_INIT') {
    	const factory = getFactory();
    	const namespace = 'com.cts.planb.blockchain.lc';
        if(txModel.approvingParty.getType() === 'Customer' && txModel.lc.approver.getType() === 'Customer'
        	&& txModel.lc.approver.getIdentifier() === txModel.approvingParty.getIdentifier()) {
            txModel.lc.status = 'SHIPPED';
            txModel.lc.approver = factory.newRelationship(namespace, 'Bank', txModel.lc.advisingBank.getIdentifier());
	    for (var i=0;i<txModel.attachments.length;i++) {
	      let attachment = txModel.attachments[i];
              attachment.status = 'SHIPPED';
              attachment.when = new Date();
	      txModel.lc.attachments.push(attachment);
	    }

            const assetRegistry = await getAssetRegistry(txModel.lc.getFullyQualifiedType());
            await assetRegistry.update(txModel.lc);
        
            const event = factory.newEvent(namespace, 'SubmitAttachmentsForReviewEvent');
            event.lc = factory.newRelationship(namespace, 'LetterOfCredit', txModel.lc.getIdentifier());
            event.approvingParty = factory.newRelationship(namespace, 'Customer', txModel.approvingParty.getIdentifier());
            emit(event);
        } else {
        	throw new Error('Access Denied, Only Supplier can submit attachments to LC and send it to advising bank.');
        }
    } else {
    	throw new Error('LC Illegal State');
    }
}
