/**
 * Write your transction processor functions here
 */
/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';
/**
 * create LC
 * @param {com.cts.planb.blockchain.lc.RequestToBank} lcreq
 * @transaction
 */
async function initateLC(lcreq) { 
    const factory = getFactory();
    const namespace = 'com.cts.planb.blockchain.lc';
    
    const letter = factory.newResource(namespace, 'LetterOfCredit', lcreq.letterId);
    letter.retailer = factory.newRelationship(namespace, 'Customer', lcreq.retailer.getIdentifier());
    letter.supplier = factory.newRelationship(namespace, 'Customer', lcreq.supplier.getIdentifier());
    letter.issuingBank = factory.newRelationship(namespace, 'Bank', lcreq.retailer.bank.getIdentifier());
    letter.advisingBank = factory.newRelationship(namespace, 'Bank', lcreq.supplier.bank.getIdentifier());
    letter.po = lcreq.po;
    letter.approver = factory.newRelationship(namespace, 'Bank', lcreq.retailer.bank.getIdentifier());
    letter.status = 'AWAITING_LC_CREATION';
    lcreq.attachment.when = new Date();
    lcreq.attachment.status = 'AWAITING_LC_CREATION';
    letter.attachments=[lcreq.attachment];

    const assetRegistry = await getAssetRegistry(letter.getFullyQualifiedType());
    await assetRegistry.add(letter);

    const lcReqEvent = factory.newEvent(namespace, 'RequestToBankEvent');
    lcReqEvent.lc = letter;
    lcReqEvent.approvingParty = factory.newRelationship(namespace, 'Customer', lcreq.retailer.getIdentifier());
    emit(lcReqEvent);
}    

/**
 * create LC
 * @param {com.cts.planb.blockchain.lc.LCAccepted} lcAccepted
 * @transaction
 */
async function lcAccepted(lcAccepted) {
    const factory = getFactory();
    const namespace = 'com.cts.planb.blockchain.lc';
     
    let lc = lcAccepted.lc;
    if(lc.status !== 'AWAITING_LC_CREATION' && lc.status !== 'LC_AMEND'){
	throw new Error('LC Illegal State');
    }
       

    if(lcAccepted.approvingParty.getType() === 'Bank'&&
        lc.approver.getType() === 'Bank' && 
        lc.approver.getIdentifier() === lcAccepted.approvingParty.getIdentifier()
        ){
	
        lc.approver = factory.newRelationship(namespace, 'Bank', lc.advisingBank.getIdentifier());
        lc.status = 'LC_REQ_INIT';
        lc.instructions = lcAccepted.instructions;
	if(lcAccepted.attachment!=null){
            lcAccepted.attachment.status = 'LC_REQ_INIT';
            lcAccepted.attachment.when = new Date();
            lc.attachments.push(lcAccepted.attachment);
        }

        const assetRegistry = await getAssetRegistry(lc.getFullyQualifiedType());
        await assetRegistry.update(lc);
        
        const lcAccAdviceEvent = factory.newEvent(namespace, 'LCInstructionsToAdvisingBankEvent');
        lcAccAdviceEvent.lc = lc;
        lcAccAdviceEvent.approvingParty = factory.newRelationship(namespace, 'Bank', lc.advisingBank.getIdentifier());
        emit(lcAccAdviceEvent);	

        const lcAccRetailEvent = factory.newEvent(namespace, 'LCAcceptedEvent');
        lcAccRetailEvent.lc = lc;
        lcAccRetailEvent.approvingParty = factory.newRelationship(namespace, 'Bank', lc.advisingBank.getIdentifier());
        emit(lcAccRetailEvent);
    }else{
        throw new Error('Access Denied, only issuing bank can Accept LC');
    }
}

/**
 * create LC
 * @param {com.cts.planb.blockchain.lc.AmendLC} amendLC
 * @transaction
 */
async function amendLC(amendLC) {
    const factory = getFactory();
    const namespace = 'com.cts.planb.blockchain.lc';

    let lc = amendLC.lc;
    if(lc.status !== 'LC_REQ_INIT'){
        throw new Error('LC Illegal State, Cannot be ammeneded in the current state');
    }

    if(amendLC.approvingParty.getType() === 'Bank'&&
        lc.approver.getType() === 'Bank' &&
        lc.approver.getIdentifier() === amendLC.approvingParty.getIdentifier()
        ){

        lc.approver = factory.newRelationship(namespace, 'Bank', lc.issuingBank.getIdentifier());
        lc.status = 'LC_AMEND';
	lc.latestComment = amendLC.amendReason;

        const assetRegistry = await getAssetRegistry(lc.getFullyQualifiedType());
        await assetRegistry.update(lc);

        const lcEvent = factory.newEvent(namespace, 'AmendLCEvent');
        lcEvent.lc = lc;
        lcEvent.amendReqParty = factory.newRelationship(namespace, 'Bank', amendLC.approvingParty.getIdentifier());
        lcEvent.amendReason = amendLC.amendReason;
        emit(lcEvent);

    }else{
        throw new Error('Access Denied, only advising bank can request for LC Amendment');
    }
}

/**
 * create LC
 * @param {com.cts.planb.blockchain.lc.LCCancel} cancelLC
 * @transaction
 */
async function cancelLC(cancelLC) {
    const factory = getFactory();
    const namespace = 'com.cts.planb.blockchain.lc';

    let lc = cancelLC.lc;

    if(
        (lc.retailer.getIdentifier() === cancelLC.approvingParty.getIdentifier()
	|| lc.issuingBank.getIdentifier() === cancelLC.approvingParty.getIdentifier()) &&
	lc.status === 'AWAITING_LC_CREATION'
        ){

        lc.approver = null;
        lc.status = 'CANCEL';
	lc.latestComment = cancelLC.cancelReason;

        const assetRegistry = await getAssetRegistry(lc.getFullyQualifiedType());
        await assetRegistry.update(lc);

        const lcEvent = factory.newEvent(namespace, 'LCCancelEvent');
        lcEvent.lc = lc;
        lcEvent.cancelParty = factory.newRelationship(namespace, 'Customer', cancelLC.approvingParty.getIdentifier());
        lcEvent.cancelReason = cancelLC.cancelReason;
        emit(lcEvent);

    }else{
        throw new Error('Access Denied, only retailer or Issuing Bank can Cancel LC');
    }
}
