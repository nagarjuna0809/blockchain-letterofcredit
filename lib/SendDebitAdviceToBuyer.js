/**
* Issuing bank sends Debit advise to Buyer (Retailer)
* 
* @param {com.cts.planb.blockchain.lc.SendDebitAdviceToBuyer} sendDebitAdviceToBuyer
* @transaction
*/
async function sendDebitAdviceToBuyer(txModel) {
    if(txModel.lc.status === 'PAYMENT_ADVICE') {
    	const factory = getFactory();
    	const namespace = 'com.cts.planb.blockchain.lc';
        if(txModel.approvingParty.getType() === 'Bank' && txModel.lc.approver.getType() === 'Bank'
        	&& txModel.lc.approver.getIdentifier() === txModel.approvingParty.getIdentifier()) {
            txModel.lc.status = 'PAYMENT_REQ';
            txModel.lc.approver = factory.newRelationship(namespace, 'Customer', txModel.lc.retailer.getIdentifier());
            const assetRegistry = await getAssetRegistry(txModel.lc.getFullyQualifiedType());
            await assetRegistry.update(txModel.lc);
        
            const event = factory.newEvent(namespace, 'SendDebitAdviceToBuyerEvent');
            event.lc = factory.newRelationship(namespace, 'LetterOfCredit', txModel.lc.getIdentifier());
            event.approvingParty = factory.newRelationship(namespace, 'Bank', txModel.approvingParty.getIdentifier());
            emit(event);
        } else {
        	throw new Error('Access Denied, Only Issuing bank can send Debit advise to Buyer (Retailer).');
        }
    } else {
    	throw new Error('LC Illegal State');
    }
}
