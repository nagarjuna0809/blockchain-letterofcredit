/**
* Advising bank issues Debit advise to Issuing Bank and sends payment to Supplier account
* 
* @param {com.cts.planb.blockchain.lc.DebitAdviceToIssuingBank} debitAdviceToIssuingBank
* @transaction
*/
async function debitAdviceToIssuingBank(txModel) {
    if(txModel.lc.status === 'SHIPPED') {
    	const factory = getFactory();
    	const namespace = 'com.cts.planb.blockchain.lc';
        if(txModel.approvingParty.getType() === 'Bank' && txModel.lc.approver.getType() === 'Bank'
        	&& txModel.lc.approver.getIdentifier() === txModel.approvingParty.getIdentifier()) {
            txModel.lc.status = 'PAYMENT_ADVICE';
            txModel.lc.approver = factory.newRelationship(namespace, 'Bank', txModel.lc.issuingBank.getIdentifier());
            const assetRegistry = await getAssetRegistry(txModel.lc.getFullyQualifiedType());
            await assetRegistry.update(txModel.lc);
        
            const event = factory.newEvent(namespace, 'DebitAdviceToIssuingBankEvent');
            event.lc = factory.newRelationship(namespace, 'LetterOfCredit', txModel.lc.getIdentifier());
            event.approvingParty = factory.newRelationship(namespace, 'Bank', txModel.approvingParty.getIdentifier());
            emit(event);
            const event1 = factory.newEvent(namespace, 'PaymentIntoSupplierAccEvent');
            event1.supplier = factory.newRelationship(namespace, 'Customer', txModel.lc.supplier.getIdentifier());
            event1.approvingParty = factory.newRelationship(namespace, 'Bank', txModel.approvingParty.getIdentifier());
            emit(event1);
        } else {
        	throw new Error('Access Denied, Only Advising bank can issue Debit advise to Issuing Bank and can send payment to Supplier account.');
        }
    } else {
    	throw new Error('LC Illegal State');
    }
}
