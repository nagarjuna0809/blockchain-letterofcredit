/**
* Advising bank passes LC instructions to Beneficiary (Supplier)
* 
* @param {com.cts.planb.blockchain.lc.LCPassInstructionsToBeneficiary} lcPassInstructionsToBeneficiary
* @transaction
*/
async function lcPassInstructionsToBeneficiary(txModel) {
    if(txModel.lc.status === 'LC_REQ_INIT') {
    	const factory = getFactory();
    	const namespace = 'com.cts.planb.blockchain.lc';
        if(txModel.approvingParty.getType() === 'Bank' && txModel.lc.approver.getType() === 'Bank'
        	&& txModel.lc.approver.getIdentifier() === txModel.approvingParty.getIdentifier()) {
            txModel.lc.status = 'SHIPMENT_INIT';
	    if(txModel.attachment!=null){
		txModel.attachment.status = 'SHIPMENT_INIT';
		txModel.attachment.when = new Date();
                txModel.lc.attachments.push(txModel.attachment);
	    }
            txModel.lc.approver = factory.newRelationship(namespace, 'Customer', txModel.lc.supplier.getIdentifier());
            const assetRegistry = await getAssetRegistry(txModel.lc.getFullyQualifiedType());
            await assetRegistry.update(txModel.lc);
        
            const event = factory.newEvent(namespace, 'LCPassInstructionsToBeneficiaryEvent');
            event.lc = factory.newRelationship(namespace, 'LetterOfCredit', txModel.lc.getIdentifier());
            event.approvingParty = factory.newRelationship(namespace, 'Bank', txModel.approvingParty.getIdentifier());
            emit(event);
        } else {
        	throw new Error('Access Denied, Only Advising Bank can pass instructions to beneficiary.');
        }
    } else {
    	throw new Error('LC Illegal State');
    }
}
